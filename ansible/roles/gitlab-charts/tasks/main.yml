---
- name: Configure kubeconfig credentials
  import_tasks: kubeconfig.yml
  tags:
    - reconfigure
    - charts
    - charts_values
    - kubeconfig

- name: Configure chart secrets
  import_tasks: secrets.yml
  tags:
    - reconfigure
    - charts

- name: Run custom chart tasks
  block:
    - name: Check if Custom Chart tasks file exists
      stat:
        path: "{{ gitlab_charts_custom_tasks_file }}"
      register: gitlab_charts_custom_tasks_file_path

    - name: Run Custom Chart tasks
      include_tasks: "{{ gitlab_charts_custom_tasks_file }}"
      when: gitlab_charts_custom_tasks_file_path.stat.exists
  tags:
    - reconfigure
    - charts

- name: Gather facts for Omnibus Postgres cluster
  block:
    - name: Get latest Postgres Leader
      command: gitlab-ctl get-postgresql-primary
      register: postgres_leader_int_address
      delegate_to: "{{ groups['postgres'][0] }}"
      delegate_facts: true
      become: true

    - name: Set Postgres Leader IP and Port
      set_fact:
        postgres_host: "{{ postgres_leader_int_address.stdout.split(':')[0] }}"
        postgres_port: "{{ postgres_leader_int_address.stdout.split(':')[1] }}"
  when:
    - "'postgres' in groups and groups['postgres'] | length > 1"
    - postgres_replication_manager == 'patroni'
  tags:
    - reconfigure
    - charts

- name: Non Omnibus database related tasks
  block:
    - name: Create Database User
      community.postgresql.postgresql_user:
        login_host: "{{ postgres_host }}"
        login_port: "{{ postgres_port }}"
        login_user: "{{ postgres_admin_username }}"
        login_password: "{{ postgres_admin_password }}"
        db: "template1"
        name: "{{ postgres_username.split('@')[0] }}"
        password: "{{ postgres_password }}"
        role_attr_flags: CREATEDB
        expires: infinity

    - name: Create Database
      community.postgresql.postgresql_db:
        login_host: "{{ postgres_host }}"
        login_port: "{{ postgres_port }}"
        login_user: "{{ postgres_username }}"
        login_password: "{{ postgres_password }}"
        name: "{{ postgres_database_name }}"
        encoding: UTF-8

    - name: Enable required Postgres extensions
      community.postgresql.postgresql_ext:
        login_host: "{{ postgres_host }}"
        login_port: "{{ postgres_port }}"
        login_user: "{{ postgres_admin_username }}"
        login_password: "{{ postgres_admin_password }}"
        name: "{{ item }}"
        db: "{{ postgres_database_name }}"
      loop: ['pg_trgm', 'btree_gist', 'plpgsql']
  when: postgres_external
  tags:
    - reconfigure
    - db_migrate
  run_once: true

- name: Gather facts for Node Pools
  block:
    - name: Gather all Node Pool Info
      kubernetes.core.k8s_info:
        kind: Node
      register: node_info

    - name: Set Node Pool capacity facts
      set_fact:
        webservice_cpus: "{{ node_info.resources | selectattr('metadata.labels.workload', 'equalto', 'webservice') | map(attribute='status.capacity.cpu') | map('int') | sum }}"
        webservice_memory: "{{ node_info.resources | selectattr('metadata.labels.workload', 'equalto', 'webservice') | map(attribute='status.capacity.memory') | map('regex_replace', '[a-zA-Z]') | map('int') | sum }}"
        sidekiq_cpus: "{{ node_info.resources | selectattr('metadata.labels.workload', 'equalto', 'sidekiq') | map(attribute='status.capacity.cpu') | map('int') | sum }}"
        sidekiq_memory: "{{ node_info.resources | selectattr('metadata.labels.workload', 'equalto', 'sidekiq') | map(attribute='status.capacity.memory') | map('regex_replace', '[a-zA-Z]') | map('int') | sum }}"

    - name: Set Webservice CPU and memory resources
      set_fact:
        # Configure larger amount of workers for hybrid environments bigger than 2k based on available CPUs
        webservice_requests_cpu: "{{ gitlab_charts_webservice_requests_cpu if (webservice_cpus | int) > 25 else 2 }}"
        webservice_requests_memory_gb: "{{ gitlab_charts_webservice_requests_memory_gb if (webservice_cpus | int) > 25 else 2.5 }}"
        webservice_limits_memory_gb: "{{ gitlab_charts_webservice_limits_memory_gb if (webservice_cpus | int) > 25 else 2.6 }}"

    - name: Set Pod Counts
      set_fact:
        # Calculate maximum pod count by either the max that can fit in 93.5% of available CPUs or Memory
        # Memory: K8s reports memory in KiB which needs to be converted to GB
        # CPU: Can be fractional, e.g. 0.5, so converted to whole numbers by multiplying by a 100 to allow correct division
        webservice_pods: "{{ [((webservice_memory | int) * 1.024 / 1024 / 1024 / (webservice_limits_memory_gb | float) * 0.935), ((webservice_cpus | int * 100) / (webservice_requests_cpu | float  * 100) * 0.935)] | map('int') | min }}"
        sidekiq_pods: "{{ [((sidekiq_memory | int) * 1.024 / 1024 / 1024 / gitlab_charts_sidekiq_limits_memory_gb * 0.935), ((sidekiq_cpus | int * 100) / (gitlab_charts_sidekiq_requests_cpu * 100) * 0.935)] | map('int') | min }}"

    - name: Show calculated numbers for charts
      debug:
        msg: |
          webservice_cpus: {{ webservice_cpus }}
          webservice_memory: {{ webservice_memory }}
          sidekiq_cpus: {{ sidekiq_cpus }}
          sidekiq_memory: {{ sidekiq_memory }}

          webservice_requests_cpu: {{ webservice_requests_cpu }}
          webservice_requests_memory_gb: {{ webservice_requests_memory_gb }}
          webservice_limits_memory_gb: {{ webservice_limits_memory_gb }}
          webservice_pods_by_memory: {{ (webservice_memory | int) * 1.024 / 1024 / 1024 / (webservice_limits_memory_gb | float) * 0.935 }}
          webservice_pods_by_cpu: {{ (webservice_cpus | int * 100) / (webservice_requests_cpu | float  * 100) * 0.935 }}

          sidekiq_requests_cpu: {{ gitlab_charts_sidekiq_requests_cpu }}
          sidekiq_requests_memory_gb: {{ gitlab_charts_sidekiq_requests_memory_gb }}
          sidekiq_limits_memory_gb: {{ gitlab_charts_sidekiq_limits_memory_gb }}
          sidekiq_pods_by_memory: {{ (sidekiq_memory | int) * 1.024 / 1024 / 1024 / gitlab_charts_sidekiq_limits_memory_gb * 0.935 }}
          sidekiq_pods_by_cpu: {{ (sidekiq_cpus | int * 100) / (gitlab_charts_sidekiq_requests_cpu * 100) * 0.935 }}

          webservice_pods: {{ webservice_pods }}
          sidekiq_pods: {{ sidekiq_pods }}
      when: gitlab_charts_show_values
  tags:
    - reconfigure
    - charts
    - charts_values

- name: Add GitLab Charts repo
  kubernetes.core.helm_repository:
    name: gitlab
    repo_url: "https://charts.gitlab.io/"
  tags:
    - reconfigure
    - charts

- name: Get GitLab Charts version if App version specified
  # Helm doesn't allow installs by app_version - https://github.com/helm/helm/issues/8194
  block:
    - name: Get all GitLab Charts versions
      command: helm search repo gitlab/gitlab -l -o json
      register: gitlab_charts_versions

    - name: Match GitLab Charts version to App version
      set_fact:
        gitlab_charts_version: "{{ (gitlab_charts_versions.stdout | from_json | selectattr('name', 'equalto', 'gitlab/gitlab') | selectattr('app_version', 'equalto', gitlab_version))[0].version }}"

    - name: Show GitLab Charts Version
      debug:
        msg: "Charts version for {{ gitlab_version }} is {{ gitlab_charts_version }}"
  when: gitlab_version != ""
  tags:
    - reconfigure
    - charts
    - charts_version

# To set task runner/toolbox pod based on GitLab app version in Charts
- name: Get GitLab App version if version is not specified
  block:
    - name: Get all GitLab Charts versions
      command: helm search repo gitlab/gitlab -l -o json
      register: gitlab_charts_versions

    - name: Get latest GitLab app version from Charts
      set_fact:
        gitlab_version: "{{ (gitlab_charts_versions.stdout | from_json | selectattr('name', 'equalto', 'gitlab/gitlab'))[0].app_version }}"
  when: gitlab_version == ""
  tags:
    - reconfigure
    - charts
    - charts_version

- name: Lookup GitLab Chart values
  set_fact:
    gitlab_charts_values: "{{ lookup('template', 'templates/gitlab.yml.j2') | from_yaml }}"
  tags:
    - reconfigure
    - charts
    - charts_values

- name: Check if custom GitLab Chart values are provided
  stat:
    path: "{{ gitlab_charts_custom_config_file }}"
  register: custom_config_file
  tags:
    - reconfigure
    - charts
    - charts_values

- name: Merge in custom GitLab Chart values if provided
  set_fact:
    gitlab_charts_values: "{{ gitlab_charts_values | combine(lookup('file', gitlab_charts_custom_config_file) | from_yaml, recursive=True) }}"
  when: custom_config_file.stat.exists
  tags:
    - reconfigure
    - charts
    - charts_values

- name: Show charts values if configured
  debug:
    msg: "{{ gitlab_charts_values }}"
  tags:
    - reconfigure
    - charts
    - charts_values
  when: gitlab_charts_show_values

- name: Install GitLab Charts
  kubernetes.core.helm:
    name: gitlab
    chart_ref: gitlab/gitlab
    chart_version: "{{ gitlab_charts_version | default(None) }}"
    update_repo_cache: true
    release_namespace: "{{ gitlab_charts_release_namespace }}"
    values: "{{ gitlab_charts_values }}"
  tags:
    - reconfigure
    - charts

# Comes as default in GCP, needs to be specifically added in AWS
- name: Add Metrics server for AWS clusters
  block:
    - name: Download metrics-server manifest.
      ansible.builtin.get_url:
        url: https://github.com/kubernetes-sigs/metrics-server/releases/latest/download/components.yaml
        dest: /tmp/metrics-server.yaml
        mode: '0664'

    - name: Apply metrics-server manifest to the cluster.
      kubernetes.core.k8s:
        state: present
        src: /tmp/metrics-server.yaml
  when: cloud_provider == 'aws'
  tags: metric-server
